#pragma once
#ifndef FILE_READER_H
#define FILE_READER_H

#include "conference-program.h"

void read(const char* file_name, conference_program* array[], int& size);

#endif
