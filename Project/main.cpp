#include <iostream>
#include <iomanip>
using namespace std;
#include "conference-program.h"
#include "file_reader.h"
#include "constans.h"
#include "filter.h"
#include <windows.h>

void output(conference_program* subscriptions)
{
    /********** вывод спикера **********/
    cout << "Speaker........: ";
    // вывод фамилии
    cout << subscriptions->reader.last_name << ' ';
    // вывод первой буквы имени
    cout << subscriptions->reader.first_name[0] << '.';
    // вывод первой буквы отчества
    cout << subscriptions->reader.middle_name[0] << '\n';
    cout << '\n';
    // вывод названия
    cout << subscriptions->title << '\n';
    cout << '\n';
    /********** вывод начала доклада **********/
    // вывод часов
    cout << setw(2) << setfill('0') << subscriptions->start.hour << ':';
    // вывод минут
    cout << setw(2) << setfill('0') << subscriptions->start.minute << '\n';
    cout << '\n';
    /********** вывод конца доклада **********/
    // вывод часов
    cout << setw(2) << setfill('0') << subscriptions->finish.hour << ':';
    // вывод минут
    cout << setw(2) << setfill('0') << subscriptions->finish.minute << '\n';
    cout << '\n';
    cout << '\n';
}
int main()
{
    SetConsoleCP(1251);
    SetConsoleOutputCP(1251);
    cout << "Test work 8. GIT\n";
    cout << "Variant №2. Programm conference\n";
    cout << "Author:Andrey Zhvikau\n\n";
    cout << "Group: 11\n";
    conference_program* subscriptions[MAX_FILE_ROWS_COUNT];
    int size;
    try
    {
        read("data.txt", subscriptions, size);
        cout << "***** Programm conference *****\n\n";
        for (int i = 0; i < size; i++)
        {
            output(subscriptions[i]);
        }
        bool (*check_function)(conference_program*) = NULL;
        int item;
        cin >> item;
        cout << '\n';
        switch (item)
        {
        case 1:
            check_function = check_by_author;
            cout << "*****Filter from LFS*****\n\n";
            break;
        case 2:
            check_function = check_by_time;
            cout << "*****Report > 15*****\n\n";
            break;
        default:
            throw "  ";
        }
        if (check_function)
        {
            int new_size;
            conference_program** filtered = filter(subscriptions, size, check_function, new_size);
            for (int i = 0; i < new_size; i++)
            {
                output(filtered[i]);
            }
            delete[] filtered;
        }
        for (int i = 0; i < size; i++)
        {
            delete subscriptions[i];
        }
    }
    catch (const char* error)
    {
        cout << error << '\n';
    }
    return 0;
}
